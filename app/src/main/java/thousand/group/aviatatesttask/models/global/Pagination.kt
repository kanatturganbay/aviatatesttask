package thousand.group.aviatatesttask.models.global

import com.google.gson.JsonElement

data class Pagination(
    var data: JsonElement? = null
)