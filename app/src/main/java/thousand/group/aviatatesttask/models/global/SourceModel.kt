package thousand.group.aviatatesttask.models.global

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SourceModel(
    var id: String? = null,
    var name: String? = null
) : Parcelable