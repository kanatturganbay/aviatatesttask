package thousand.group.aviatatesttask.models.global

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize
import thousand.group.aviatatesttask.global.db.type_converters.DataConverter

@Entity(
    tableName = "news_model",
    indices = [Index(
        value = ["_id"],
        unique = true
    )]
)
@TypeConverters(DataConverter::class)
@Parcelize
data class NewsModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    var id: Int = 0,

    @ColumnInfo(name = "source")
    var source: SourceModel? = null,

    @ColumnInfo(name = "author")
    var author: String? = null,

    @ColumnInfo(name = "title")
    var title: String? = null,

    @ColumnInfo(name = "description")
    var description: String? = null,

    @ColumnInfo(name = "url")
    var url: String? = null,

    @ColumnInfo(name = "urlToImage")
    var urlToImage: String? = null,

    @ColumnInfo(name = "publishedAt")
    var publishedAt: String? = null,

    @ColumnInfo(name = "content")
    var content: String? = null,

    @Ignore
    var isChecked: Boolean = false,

    @Ignore
    var isOpened:Boolean = false

) : Parcelable