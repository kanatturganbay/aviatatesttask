package thousand.group.aviatatesttask.models.global

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsTotalResult(
    var articles: MutableList<NewsModel>
) : Parcelable