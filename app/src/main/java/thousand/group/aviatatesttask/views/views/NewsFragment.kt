package thousand.group.aviatatesttask.views.views

import android.os.Bundle
import androidx.fragment.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.fragment_news.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.adapter.FragmentPagerAdapter
import thousand.group.aviatatesttask.global.base.BaseFragment
import thousand.group.aviatatesttask.global.extentions.getSupportFragmentManager
import thousand.group.aviatatesttask.global.extentions.observeLiveData
import thousand.group.aviatatesttask.global.extentions.replaceFragmentWithBackStack
import thousand.group.aviatatesttask.global.helpers.MainFragmentHelper
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.aviatatesttask.views.view_models.NewsViewModel

class NewsFragment : BaseFragment<NewsViewModel>(NewsViewModel::class) {
    override var layoutResId = R.layout.fragment_news

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorWhite,
            isShowBottomNavBar = true,
            posBottomNav = 0
        )
    )

    companion object {
        fun newInstance(): NewsFragment {
            val fragment = NewsFragment()
            val args = Bundle()
            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var pagerAdapter: FragmentPagerAdapter

    override fun initView(savedInstanceState: Bundle?) {
        setViewPager()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.openDetailFragmentLV) {
            openNewsDetailFragment(it)
        }
    }

    override fun initController() {

    }

    override fun onResume() {
        super.onResume()
    }

    private fun setViewPager() {
        view_pager.removeAllViews()

        if (!::pagerAdapter.isInitialized) {
            pagerAdapter = FragmentPagerAdapter(
                childFragmentManager,
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
            )

            pagerAdapter.addFragment(
                TopNewsFragment.newInstance(),
                getString(R.string.label_top_news)
            )

            pagerAdapter.addFragment(
                AllNewsFragment.newInstance(),
                getString(R.string.label_other_news)
            )
        }

        view_pager.adapter = pagerAdapter
        view_pager.offscreenPageLimit = 2

        news_type_tab.setupWithViewPager(view_pager)
    }

    private fun openNewsDetailFragment(model: NewsModel) {
        val fragment = NewsDetailFragment.newInstance(model)

        getSupportFragmentManager()
            .replaceFragmentWithBackStack(
                R.id.fragment_container,
                fragment,
                fragment.fragmentMainTag
            )
    }

}