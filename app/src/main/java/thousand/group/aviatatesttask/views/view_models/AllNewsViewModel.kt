package thousand.group.aviatatesttask.views.view_models

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import thousand.group.aviatatesttask.global.base.BaseViewModel
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.helpers.Paginator
import thousand.group.aviatatesttask.global.helpers.ResErrorHelper
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.global.system.SingleLiveEvent
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.aviatatesttask.views.repositories.news.NewsRepository

class AllNewsViewModel(
    override val context: Context,
    val repository: NewsRepository,
    override val resourceManager: ResourceManager,
    override val globalReceiver: GlobalReceiver,
    override var args: Bundle?
) : BaseViewModel(
    context,
    resourceManager,
    globalReceiver,
    args,
    intentActionsList = mutableListOf(

    )
) {
    val showSwipeLV = MutableLiveData<Boolean>()
    val showNothingTextLV = MutableLiveData<Boolean>()
    val setAdapterLV = MutableLiveData<Boolean>()

    val setListLV = SingleLiveEvent<MutableList<NewsModel>>()
    val showBottomProgressLV = SingleLiveEvent<Boolean>()
    val setItemToList = SingleLiveEvent<Pair<NewsModel, Int>>()

    private var startLoad = false

    private var selectedItemsList = mutableListOf<NewsModel>()

    private val paginator = Paginator({
        repository.getAllNews(
            it,
            AppConstants.ApiValues.api_key,
            AppConstants.ApiValues.q
        )
    },
        object : Paginator.ViewController<NewsModel> {
            override fun showEmptyProgress(show: Boolean) {
            }

            override fun showEmptyError(show: Boolean, error: Throwable?) {
                startOnError(error)
            }

            override fun showEmptyView(show: Boolean) {
                showNothingTextLV.postValue(show)
            }

            override fun showData(show: Boolean, data: MutableList<NewsModel>) {
                Log.i(vmTag, data.toString())
                Log.i(vmTag, selectedItemsList.toString())

                data.forEach { newsModel ->
                    selectedItemsList.forEach {
                        if (newsModel.publishedAt.equals(it.publishedAt)) {
                            newsModel.isChecked = true
                            return@forEach
                        }
                    }
                }

                setListLV.postValue(data)
                showBottomProgressLV.postValue(false)
                showSwipeLV.postValue(false)

                startLoad = true
            }

            override fun showErrorMessage(error: Throwable) {
                startOnError(error)
            }

            override fun showRefreshProgress(show: Boolean) {
            }

            override fun showPageProgress(show: Boolean) {
                showBottomProgressLV.postValue(show)
            }

        }
    )

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

    override fun parseBundle(args: Bundle?) {
    }

    override fun onStart() {
        fillSelectedList()
        setAdapterLV.postValue(true)
        loadStart()
    }

    override fun onFinish() {
        showBottomProgressLV.postValue(false)
        showSwipeLV.postValue(false)
    }

    override fun onResumed() {
    }

    fun openDetailClicked(position: Int, model: NewsModel) {
        sendMessageReceiver(
            AppConstants.LocalizedActionConstants.OPEN_DETAIL_FRAGMENT,
            AppConstants.LocalizedActionConstants.OPEN_DETAIL_FRAGMENT,
            model
        )
    }

    fun loadStart() = paginator.refresh()

    fun loadNextPage() = paginator.loadNewPage()

    fun heartClicked(model: NewsModel, position: Int) {

        Log.i(vmTag, "model: ${model}, position: ${position}")

        model.publishedAt?.apply {

            Log.i(vmTag, "Position: $position publishedAt not null")

            repository
                .getCountOfNewsModel(this)
                .subscribe({
                    if (it > 0) {

                        Log.i(vmTag, "Position: $position isAdded Already")

                        repository
                            .deleteNewsModel(this)
                            .doOnSubscribe { showProgressBar(true) }
                            .doFinally { showProgressBar(false) }
                            .subscribe({

                                Log.i(vmTag, "Position: $position isDeleted Successfully")

                                showMessageSuccess("")

                                model.isChecked = false

                                setItemToList.postValue(Pair(model, position))
                                fillSelectedList()

                            }, {
                                Log.i(vmTag, "error: ${it.message}")
                            })
                            .connect()

                    } else {

                        Log.i(vmTag, "Position: $position isDeleted Already")

                        repository
                            .addNewsModel(model)
                            .doOnSubscribe { showProgressBar(true) }
                            .doFinally { showProgressBar(false) }
                            .subscribe({

                                Log.i(vmTag, "Position: $position isAdded Successfully")

                                showMessageSuccess("")

                                model.isChecked = true

                                setItemToList.postValue(Pair(model, position))
                                fillSelectedList()

                            }, {
                                Log.i(vmTag, "error: ${it.message}")
                            })
                            .connect()
                    }
                }, {
                    it.message?.apply {
                        Log.i(vmTag, "error: ${it.message}")
                    }
                })
                .connect()
        }

    }

    private fun startOnError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )
        }

        showSwipeLV.postValue(false)
        showBottomProgressLV.postValue(false)

        if (!startLoad) {
            showNothingTextLV.postValue(true)
        }
    }

    private fun fillSelectedList() {
        repository.getAllNewsModels()
            .subscribe({
                selectedItemsList.clear()
                selectedItemsList.addAll(it)
            }, {
            })
            .connect()
    }
}