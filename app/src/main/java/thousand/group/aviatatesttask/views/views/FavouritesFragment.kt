package thousand.group.aviatatesttask.views.views

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_favourites.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.adapter.FavouritesAdapter
import thousand.group.aviatatesttask.global.base.BaseFragment
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.decorations.VerticalListItemDecoration
import thousand.group.aviatatesttask.global.extensions.visible
import thousand.group.aviatatesttask.global.extentions.getSupportFragmentManager
import thousand.group.aviatatesttask.global.extentions.observeLiveData
import thousand.group.aviatatesttask.global.extentions.removeItemDecorations
import thousand.group.aviatatesttask.global.extentions.replaceFragmentWithBackStack
import thousand.group.aviatatesttask.global.helpers.MainFragmentHelper
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.aviatatesttask.views.view_models.FavouritesViewModel

class FavouritesFragment : BaseFragment<FavouritesViewModel>(FavouritesViewModel::class) {
    override var layoutResId = R.layout.fragment_favourites

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorWhite,
            isShowBottomNavBar = true,
            posBottomNav = 1
        )
    )

    companion object {

        fun newInstance(): FavouritesFragment {
            val fragment = FavouritesFragment()
            val args = Bundle()
            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: FavouritesAdapter

    override fun initView(savedInstanceState: Bundle?) {

    }

    override fun initLiveData() {
        observeLiveData(viewModel.showNothingTextLV) {
            showEmptyText(it)
        }
        observeLiveData(viewModel.setAdapterLV) {
            setAdapter()
        }
        observeLiveData(viewModel.setListLV) {
            adapter.setDataList(it)
            setDecorations()
        }
        observeLiveData(viewModel.deleteItemLV) {
            adapter.deleteItem(it)
        }
        observeLiveData(viewModel.showDeleteDialogLV) {
            showDeleteDialog(it)
        }
        observeLiveData(viewModel.openDetailFragmentLV) {
            openNewsDetailFragment(it)
        }
    }

    override fun initController() {
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = FavouritesAdapter(
                requireContext(),
                { model ->
                    viewModel.openDetailClicked(model)
                },
                { model, position ->
                    viewModel.showDeleteDialog(model, position)
                },
                {
                    showEmptyText(it)
                }
            )
        }

        fav_list.adapter = adapter
    }

    private fun showEmptyText(show: Boolean) {
        empty_text.visible(show)
        fav_list.visible(!show)
    }

    private fun setDecorations() {
        adapter.apply {
            fav_list.removeItemDecorations()

            fav_list.addItemDecoration(
                VerticalListItemDecoration(
                    resources.getDimension(R.dimen._10sdp).toInt(),
                    itemCount
                )
            )
        }
    }

    private fun showDeleteDialog(bundle: Bundle) {

        val title = bundle.getInt(AppConstants.BundleConstants.TITLE)
        val message = bundle.getInt(AppConstants.BundleConstants.DESC)
        val position = bundle.getInt(AppConstants.BundleConstants.POSITION)
        val model = bundle.getParcelable<NewsModel>(AppConstants.BundleConstants.BUNDLE_MODEL)

        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.label_yes) { dialogInterface, i ->
                viewModel.deleteNews(position, model)
                dialogInterface.dismiss()
            }
            .setNegativeButton(R.string.label_no) { dialogInterface, i ->
                dialogInterface.dismiss()
            }
            .setOnCancelListener {
                adapter.closeBasketItem(position)
            }
            .setOnDismissListener {
                adapter.closeBasketItem(position)
            }
            .show()
    }

    private fun openNewsDetailFragment(model: NewsModel) {
        val fragment = NewsDetailFragment.newInstance(model)

        getSupportFragmentManager()
            .replaceFragmentWithBackStack(
                R.id.fragment_container,
                fragment,
                fragment.fragmentMainTag
            )
    }

}