package thousand.group.aviatatesttask.views.view_models

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.MutableLiveData
import thousand.group.aviatatesttask.global.base.BaseViewModel
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.helpers.Paginator
import thousand.group.aviatatesttask.global.helpers.ResErrorHelper
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.global.system.SingleLiveEvent
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.aviatatesttask.views.repositories.news.NewsRepository

class TopNewsViewModel(
    override val context: Context,
    val repository: NewsRepository,
    override val resourceManager: ResourceManager,
    override val globalReceiver: GlobalReceiver,
    override var args: Bundle?
) : BaseViewModel(
    context,
    resourceManager,
    globalReceiver,
    args,
    intentActionsList = mutableListOf(

    )
) {

    val showSwipeLV = MutableLiveData<Boolean>()
    val showNothingTextLV = MutableLiveData<Boolean>()
    val setAdapterLV = MutableLiveData<Boolean>()

    val setListLV = SingleLiveEvent<MutableList<NewsModel>>()
    val showBottomProgressLV = SingleLiveEvent<Boolean>()
    val setItemToList = SingleLiveEvent<Pair<NewsModel, Int>>()

    private var startLoad = false

    private var selectedItemsList = mutableListOf<NewsModel>()

    private val countDownTimer = object : CountDownTimer(5000, 1000) {
        override fun onTick(millisUntilFinished: Long) {

            Log.i(vmTag, "OnTick")

            timerIsActive = true
        }

        override fun onFinish() {

            Log.i(vmTag, "onFinish")

            timerIsActive = false
            this.cancel()
            this.start()

            loadStart()
        }
    }

    private var timerIsActive = false

    private val paginator = Paginator({
        repository.getTopNews(
            it,
            AppConstants.ApiValues.api_key,
            AppConstants.ApiValues.country
        )
    },
        object : Paginator.ViewController<NewsModel> {
            override fun showEmptyProgress(show: Boolean) {
            }

            override fun showEmptyError(show: Boolean, error: Throwable?) {
                startOnError(error)
            }

            override fun showEmptyView(show: Boolean) {
                showNothingTextLV.postValue(show)
            }

            override fun showData(show: Boolean, data: MutableList<NewsModel>) {
                Log.i(vmTag, data.toString())
                Log.i(vmTag, selectedItemsList.toString())

                data.forEach { newsModel ->
                    selectedItemsList.forEach {
                        if (newsModel.publishedAt.equals(it.publishedAt)) {
                            newsModel.isChecked = true
                            return@forEach
                        }
                    }
                }

                setListLV.postValue(data)
                showBottomProgressLV.postValue(false)
                showSwipeLV.postValue(false)

                startLoad = true
            }

            override fun showErrorMessage(error: Throwable) {
                startOnError(error)
            }

            override fun showRefreshProgress(show: Boolean) {
            }

            override fun showPageProgress(show: Boolean) {
                showBottomProgressLV.postValue(show)
            }

        }
    )

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

    override fun parseBundle(args: Bundle?) {
    }

    override fun onStart() {
        fillSelectedList()
        setAdapterLV.postValue(true)
        startSwipe()
    }

    override fun onFinish() {
        showBottomProgressLV.postValue(false)
        showSwipeLV.postValue(false)
        countDownTimer.cancel()
    }

    override fun onResumed() {
        fillSelectedList()
    }

    fun openDetailClicked(position: Int, model: NewsModel) {
        sendMessageReceiver(
            AppConstants.LocalizedActionConstants.OPEN_DETAIL_FRAGMENT,
            AppConstants.LocalizedActionConstants.OPEN_DETAIL_FRAGMENT,
            model
        )
    }

    fun loadStart() {
        paginator.refresh()
    }

    fun loadNextPage() = paginator.loadNewPage()

    fun heartClicked(model: NewsModel, position: Int) {

        Log.i(vmTag, "model: ${model}, position: ${position}")

        model.publishedAt?.apply {
            repository
                .getCountOfNewsModel(this)
                .subscribe({
                    if (it > 0) {
                        repository
                            .deleteNewsModel(this)
                            .doOnSubscribe { showProgressBar(true) }
                            .doFinally { showProgressBar(false) }
                            .subscribe({
                                showMessageSuccess("")

                                model.isChecked = false

                                setItemToList.postValue(Pair(model, position))
                                fillSelectedList()
                            }, {
                                it.message?.apply {
                                    showMessageError(this)
                                }
                            })
                            .connect()

                    } else {

                        repository
                            .addNewsModel(model)
                            .doOnSubscribe { showProgressBar(true) }
                            .doFinally { showProgressBar(false) }
                            .subscribe({
                                showMessageSuccess("")

                                model.isChecked = true

                                setItemToList.postValue(Pair(model, position))
                                fillSelectedList()

                            }, {
                                it.message?.apply {
                                    showMessageError(this)
                                }
                            })
                            .connect()
                    }
                }, {
                    it.message?.apply {
                        showMessageError(this)
                    }
                })
                .connect()
        }

    }

    fun checkPosition(firstVisibleItemPosition: Int, model: NewsModel) {
        if (firstVisibleItemPosition < 2) {
            if (!timerIsActive) {
                countDownTimer.start()
            }
        } else {
            timerIsActive = false
            countDownTimer.cancel()
        }
    }

    fun startSwipe() {
        loadStart()
        countDownTimer.start()
    }

    private fun startOnError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )
        }

        showSwipeLV.postValue(false)
        showBottomProgressLV.postValue(false)

        if (!startLoad) {
            showNothingTextLV.postValue(true)
        }
    }

    private fun fillSelectedList() {
        repository.getAllNewsModels()
            .subscribe({

                Log.i(vmTag, "fillSelectedList: list -> $it")

                selectedItemsList.clear()
                selectedItemsList.addAll(it)
            }, {
                Log.i(vmTag, "error: it -> ${it.message}")
            })
            .connect()
    }

}