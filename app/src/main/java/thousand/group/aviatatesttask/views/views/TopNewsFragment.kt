package thousand.group.aviatatesttask.views.views

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_top_news.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.adapter.NewsAdapter
import thousand.group.aviatatesttask.global.base.BaseFragment
import thousand.group.aviatatesttask.global.decorations.VerticalListItemDecoration
import thousand.group.aviatatesttask.global.extensions.visible
import thousand.group.aviatatesttask.global.extentions.observeLiveData
import thousand.group.aviatatesttask.global.extentions.removeItemDecorations
import thousand.group.aviatatesttask.global.helpers.MainFragmentHelper
import thousand.group.aviatatesttask.views.view_models.TopNewsViewModel

class TopNewsFragment : BaseFragment<TopNewsViewModel>(TopNewsViewModel::class) {
    override var layoutResId = R.layout.fragment_top_news
    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorWhite,
            isShowBottomNavBar = true,
            posBottomNav = 0
        )
    )

    companion object {

        fun newInstance(): TopNewsFragment {
            val fragment = TopNewsFragment()
            val args = Bundle()
            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: NewsAdapter

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            val firstVisibleItemPosition =
                (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

            viewModel.checkPosition(
                firstVisibleItemPosition,
                adapter.getItem(0)
            )
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.setAdapterLV) {
            setAdapter()
        }
        observeLiveData(viewModel.setListLV) {
            adapter.setData(it)
            setDecorations()
        }
        observeLiveData(viewModel.showBottomProgressLV) {
            showBottomProgress(it)
        }
        observeLiveData(viewModel.showNothingTextLV) {
            showEmptyText(it)
        }
        observeLiveData(viewModel.showSwipeLV) {
            showSwipe(it)
        }
        observeLiveData(viewModel.setItemToList) {
            adapter.checkMarked(it.second, it.first)
        }
    }

    override fun initController() {
        swipe_refresh_top.setOnRefreshListener {
            viewModel.startSwipe()
        }

        top_news_list.addOnScrollListener(scrollListener)
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = NewsAdapter(
                requireContext(),
                { newsModel, i ->
                    viewModel.openDetailClicked(i, newsModel)
                },
                { newsModel, i ->
                    viewModel.heartClicked(newsModel, i)
                },
                {
                    viewModel.loadNextPage()
                },
                { show ->
                    showEmptyText(show)
                }
            )
        }

        top_news_list.adapter = adapter
    }

    private fun showSwipe(show: Boolean) {
        swipe_refresh_top.isRefreshing = show
    }

    private fun showBottomProgress(show: Boolean) = top_bottom_progress.visible(show)

    private fun showEmptyText(show: Boolean) {
        top_empty_text.visible(show)
        top_news_list.visible(!show)
        top_bottom_progress.visible(!show)
    }

    private fun setDecorations() {
        adapter.apply {
            top_news_list.removeItemDecorations()

            top_news_list.addItemDecoration(
                VerticalListItemDecoration(
                    resources.getDimension(R.dimen._10sdp).toInt(),
                    itemCount
                )
            )
        }
    }

}