package thousand.group.aviatatesttask.views.repositories.favourites

import io.reactivex.Completable
import io.reactivex.Single
import thousand.group.aviatatesttask.models.global.NewsModel

interface FavouritesRepository {
    fun deleteNewsModel(publishedDate: String): Completable

    fun getCountOfNewsModel(publishedDate: String): Single<Int>

    fun getAllNewsModels(): Single<List<NewsModel>>

}