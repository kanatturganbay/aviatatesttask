package thousand.group.aviatatesttask.views.repositories.news

import io.reactivex.Completable
import io.reactivex.Single
import thousand.group.aviatatesttask.models.global.NewsModel

interface NewsRepository {
    fun getTopNews(
        page: Int,
        apiKey: String,
        country: String
    ): Single<MutableList<NewsModel>>

    fun getAllNews(
        page: Int,
        apiKey: String,
        q: String
    ): Single<MutableList<NewsModel>>

    fun addNewsModel(newsModel: NewsModel): Completable

    fun deleteNewsModel(publishedDate: String): Completable

    fun getCountOfNewsModel(publishedDate: String): Single<Int>

    fun getAllNewsModels(): Single<List<NewsModel>>
}