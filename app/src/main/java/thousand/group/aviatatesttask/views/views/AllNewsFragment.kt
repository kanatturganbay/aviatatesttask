package thousand.group.aviatatesttask.views.views

import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.fragment_all_news.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.adapter.NewsAdapter
import thousand.group.aviatatesttask.global.base.BaseFragment
import thousand.group.aviatatesttask.global.decorations.VerticalListItemDecoration
import thousand.group.aviatatesttask.global.extensions.visible
import thousand.group.aviatatesttask.global.extentions.observeLiveData
import thousand.group.aviatatesttask.global.extentions.removeItemDecorations
import thousand.group.aviatatesttask.global.helpers.MainFragmentHelper
import thousand.group.aviatatesttask.views.view_models.AllNewsViewModel

class AllNewsFragment : BaseFragment<AllNewsViewModel>(AllNewsViewModel::class) {
    override var layoutResId = R.layout.fragment_all_news

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorWhite,
            isShowBottomNavBar = true,
            posBottomNav = 0
        )
    )

    companion object {

        fun newInstance(): AllNewsFragment {
            val fragment = AllNewsFragment()
            val args = Bundle()
            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: NewsAdapter

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.setAdapterLV) {
            setAdapter()
        }
        observeLiveData(viewModel.setListLV) {
            adapter.setData(it)
            setDecorations()
        }
        observeLiveData(viewModel.showBottomProgressLV) {
            showBottomProgress(it)
        }
        observeLiveData(viewModel.showNothingTextLV) {
            showEmptyText(it)
        }
        observeLiveData(viewModel.showSwipeLV) {
            showSwipe(it)
        }
        observeLiveData(viewModel.setItemToList) {
            adapter.checkMarked(it.second, it.first)
        }
    }

    override fun initController() {
        swipe_refresh_all.setOnRefreshListener {
            viewModel.loadStart()
        }
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = NewsAdapter(
                requireContext(),
                { newsModel, i ->
                    viewModel.openDetailClicked(i, newsModel)
                },
                { newsModel, i ->
                    viewModel.heartClicked(newsModel, i)
                    Log.i(fragmentTag, "ALL onHeart news clicked ")
                },
                {
                    viewModel.loadNextPage()
                },
                { show ->
                    showEmptyText(show)
                }
            )
        }

        all_news_list.adapter = adapter
    }

    private fun showSwipe(show: Boolean) {
        swipe_refresh_all.isRefreshing = show
    }

    private fun showBottomProgress(show: Boolean) = all_bottom_progress.visible(show)

    private fun showEmptyText(show: Boolean) {
        all_empty_text.visible(show)
        all_news_list.visible(!show)
        all_bottom_progress.visible(!show)
    }

    private fun setDecorations() {
        adapter.apply {
            all_news_list.removeItemDecorations()

            all_news_list.addItemDecoration(
                VerticalListItemDecoration(
                    resources.getDimension(R.dimen._10sdp).toInt(),
                    itemCount
                )
            )
        }
    }

}