package thousand.group.aviatatesttask.views.repositories.news

import thousand.group.aviatatesttask.global.db.data_base_util.DataBaseUtil
import thousand.group.aviatatesttask.global.services.storage_services.LocaleStorage
import thousand.group.aviatatesttask.global.system.SchedulersProvider
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.ayanproject.global.services.api_services.ServerService

class NewsRepositoryImpl(
    private val service: ServerService,
    private val dataBaseUtil: DataBaseUtil,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) : NewsRepository {

    override fun getTopNews(
        page: Int,
        apiKey: String,
        country: String
    ) = service
        .getTopNews(
            page.toString(),
            apiKey,
            country
        )
        .subscribeOn(schedulersProvider.io())
        .observeOn(schedulersProvider.ui())
        .map {
            return@map it.articles
        }

    override fun getAllNews(
        page: Int,
        apiKey: String,
        q: String
    ) = service
        .getAllNews(
            page.toString(),
            apiKey,
            q
        )
        .subscribeOn(schedulersProvider.io())
        .observeOn(schedulersProvider.ui())
        .map {
            return@map it.articles
        }

    override fun addNewsModel(newsModel: NewsModel) =
        dataBaseUtil.getDataDao().addNewsModel(newsModel)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    override fun deleteNewsModel(publishedDate: String) =
        dataBaseUtil.getDataDao().deleteNewsModel(publishedDate)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    override fun getCountOfNewsModel(publishedDate: String) =
        dataBaseUtil.getDataDao().getCountOfNewsModel(publishedDate)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    override fun getAllNewsModels() =
        dataBaseUtil.getDataDao().getAllNewsModels()
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())


}