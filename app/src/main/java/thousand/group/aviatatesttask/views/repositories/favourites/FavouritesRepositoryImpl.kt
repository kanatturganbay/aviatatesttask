package thousand.group.aviatatesttask.views.repositories.favourites

import thousand.group.aviatatesttask.global.db.data_base_util.DataBaseUtil
import thousand.group.aviatatesttask.global.services.storage_services.LocaleStorage
import thousand.group.aviatatesttask.global.system.SchedulersProvider
import thousand.group.ayanproject.global.services.api_services.ServerService

class FavouritesRepositoryImpl(
    private val service: ServerService,
    private val dataBaseUtil: DataBaseUtil,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) : FavouritesRepository {

    override fun deleteNewsModel(publishedDate: String) =
        dataBaseUtil.getDataDao().deleteNewsModel(publishedDate)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    override fun getCountOfNewsModel(publishedDate: String) =
        dataBaseUtil.getDataDao().getCountOfNewsModel(publishedDate)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    override fun getAllNewsModels() =
        dataBaseUtil.getDataDao().getAllNewsModels()
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())


}