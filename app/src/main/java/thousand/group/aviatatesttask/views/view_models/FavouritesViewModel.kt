package thousand.group.aviatatesttask.views.view_models

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.base.BaseViewModel
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.global.system.SingleLiveEvent
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.aviatatesttask.views.repositories.favourites.FavouritesRepository

class FavouritesViewModel(
    override val context: Context,
    val repository: FavouritesRepository,
    override val resourceManager: ResourceManager,
    override val globalReceiver: GlobalReceiver,
    override var args: Bundle?
) : BaseViewModel(
    context,
    resourceManager,
    globalReceiver,
    args,
    intentActionsList = mutableListOf(
    )
) {
    val showNothingTextLV = MutableLiveData<Boolean>()

    val setListLV = SingleLiveEvent<MutableList<NewsModel>>()
    val setAdapterLV = MutableLiveData<Boolean>()
    val deleteItemLV = SingleLiveEvent<Int>()
    val showDeleteDialogLV = SingleLiveEvent<Bundle>()
    val openDetailFragmentLV = SingleLiveEvent<NewsModel>()

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

    override fun parseBundle(args: Bundle?) {
    }

    override fun onStart() {
        setAdapterLV.postValue(true)
        fillSelectedList()
    }

    override fun onFinish() {
        showProgressBar(false)
    }

    override fun onResumed() {
    }

    fun deleteNews(position: Int, model: NewsModel?) {
        model?.publishedAt?.apply {
            repository
                .deleteNewsModel(this)
                .subscribe({
                    showMessageSuccess("")
                    deleteItemLV.postValue(position)
                }, {
                    it.message?.apply {
                        showMessageError(this)
                    }
                })
                .connect()
        }

    }

    fun showDeleteDialog(model: NewsModel, position: Int) {

        paramsBundle.clear()

        paramsBundle.putInt(AppConstants.BundleConstants.TITLE, R.string.label_delete)
        paramsBundle.putInt(AppConstants.BundleConstants.DESC, R.string.label_delete_desc)
        paramsBundle.putInt(AppConstants.BundleConstants.POSITION, position)
        paramsBundle.putParcelable(AppConstants.BundleConstants.BUNDLE_MODEL, model)

        showDeleteDialogLV.postValue(paramsBundle)
    }

    fun openDetailClicked(model: NewsModel) {
        if (isInternetActive()) {
            openDetailFragmentLV.postValue(model)
        } else {
            showMessageError(R.string.error_internet_connection)
        }
    }

    private fun fillSelectedList() {
        repository.getAllNewsModels()
            .doOnSubscribe { showProgressBar(true) }
            .doFinally { showProgressBar(false) }
            .subscribe({
                val list = mutableListOf<NewsModel>()
                list.addAll(it)

                setListLV.postValue(list)
            }, {
                showNothingTextLV.postValue(true)
            })
            .connect()
    }

}