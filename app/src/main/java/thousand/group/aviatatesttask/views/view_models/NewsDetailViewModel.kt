package thousand.group.aviatatesttask.views.view_models

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.base.BaseViewModel
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.global.system.SingleLiveEvent
import thousand.group.aviatatesttask.models.global.NewsModel

class NewsDetailViewModel(
    override val context: Context,
    override val resourceManager: ResourceManager,
    override val globalReceiver: GlobalReceiver,
    override var args: Bundle?
) : BaseViewModel(
    context,
    resourceManager,
    globalReceiver,
    args,
    intentActionsList = mutableListOf(
    )
) {

    val setNewsParams = MutableLiveData<Bundle>()

    val openBrowserFragmentLV = SingleLiveEvent<Intent>()

    private var model: NewsModel? = null

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

    override fun parseBundle(args: Bundle?) {
        args?.apply {
            model = getParcelable(AppConstants.BundleConstants.BUNDLE_MODEL)
        }
    }

    override fun onStart() {
        setParams()
    }

    override fun onFinish() {
    }

    override fun onResumed() {
    }

    fun onFullBtnClicked() {
        if (isInternetActive()) {
            model?.url?.apply {

                val defaultBrowser =
                    Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_BROWSER)
                defaultBrowser.data = Uri.parse(this)

                openBrowserFragmentLV.postValue(defaultBrowser)
            }
        } else {
            showMessageError(R.string.error_internet_connection)
        }
    }

    private fun setParams() {
        model?.apply {
            paramsBundle.putString(AppConstants.BundleConstants.AVATAR, urlToImage)
            paramsBundle.putString(AppConstants.BundleConstants.TITLE, title)
            paramsBundle.putString(AppConstants.BundleConstants.DESC, description)
            paramsBundle.putString(AppConstants.BundleConstants.AUTHOR, author)
            paramsBundle.putString(AppConstants.BundleConstants.CONTENT, content)

            setNewsParams.postValue(paramsBundle)
        }
    }

}