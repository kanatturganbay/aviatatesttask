package thousand.group.aviatatesttask.views.views

import android.os.Bundle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_news_detail.*
import kotlinx.android.synthetic.main.toolbar_back_icon.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.base.BaseFragment
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.constants.simple.AppConstants.BundleConstants.AUTHOR
import thousand.group.aviatatesttask.global.constants.simple.AppConstants.BundleConstants.AVATAR
import thousand.group.aviatatesttask.global.constants.simple.AppConstants.BundleConstants.CONTENT
import thousand.group.aviatatesttask.global.constants.simple.AppConstants.BundleConstants.DESC
import thousand.group.aviatatesttask.global.constants.simple.AppConstants.BundleConstants.TITLE
import thousand.group.aviatatesttask.global.extentions.observeLiveData
import thousand.group.aviatatesttask.global.helpers.MainFragmentHelper
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.aviatatesttask.views.view_models.NewsDetailViewModel

class NewsDetailFragment : BaseFragment<NewsDetailViewModel>(NewsDetailViewModel::class) {

    override var layoutResId = R.layout.fragment_news_detail

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorWhite,
            isShowBottomNavBar = false
        )
    )

    companion object {
        fun newInstance(model: NewsModel): NewsDetailFragment {
            val fragment = NewsDetailFragment()
            val args = Bundle()

            //passing arguments
            args.putParcelable(AppConstants.BundleConstants.BUNDLE_MODEL, model)

            fragment.arguments = args
            return fragment
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.setNewsParams) {
            setParams(it)
        }
        observeLiveData(viewModel.openBrowserFragmentLV) {
            startActivity(it)
        }
    }

    override fun initController() {
        back_icon.setOnClickListener {
            requireActivity().onBackPressed()
        }

        full_btn.setOnClickListener {
            viewModel.onFullBtnClicked()
        }
    }

    private fun setParams(it: Bundle) {
        Picasso.get()
            .load(it.getString(AVATAR))
            .error(R.drawable.ic_error)
            .resizeDimen(R.dimen._150sdp, R.dimen._120sdp)
            .into(imageView)

        title_full.setText(it.getString(TITLE))
        desc_full.setText(it.getString(DESC))
        author_full.setText(it.getString(AUTHOR))
        content_view_full.setText(it.getString(CONTENT))
    }

}