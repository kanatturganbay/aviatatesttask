package thousand.group.aviatatesttask.views.repositories.activity

import thousand.group.aviatatesttask.global.services.storage_services.LocaleStorage
import thousand.group.aviatatesttask.global.system.SchedulersProvider
import thousand.group.ayanproject.global.services.api_services.ServerService

class MainRepositoryImpl(
    private val service: ServerService,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) : MainRepository {

}