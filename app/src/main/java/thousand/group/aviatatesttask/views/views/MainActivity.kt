package thousand.group.aviatatesttask.views.views

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.base.BaseActivity
import thousand.group.aviatatesttask.global.extensions.visible
import thousand.group.aviatatesttask.global.extentions.clearAndReplaceFragment
import thousand.group.aviatatesttask.global.extentions.setChecked
import thousand.group.aviatatesttask.global.extentions.uncheckAllItems
import thousand.group.aviatatesttask.global.helpers.MainFragmentHelper
import thousand.group.aviatatesttask.views.view_models.MainViewModel

class MainActivity : BaseActivity<MainViewModel>(MainViewModel::class) {
    override var layoutResId = R.layout.activity_main

    override fun initIntent(intent: Intent?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
        val fr = NewsFragment.newInstance()

        supportFragmentManager.clearAndReplaceFragment(
            R.id.fragment_container,
            fr,
            fr.fragmentMainTag
        )
    }

    override fun initLiveData() {
    }

    override fun initController() {
        bottomNavMain.setOnNavigationItemSelectedListener {
            supportFragmentManager.apply {

                val fragment: Fragment?

                if (!this.fragments.isNullOrEmpty()) {
                    fragment = fragments[fragments.size - 1]
                } else {
                    fragment = null
                }

                when (it.itemId) {
                    R.id.navigation_home -> {
                        if (fragment !is NewsFragment) {
                            val fr = NewsFragment.newInstance()

                            supportFragmentManager.clearAndReplaceFragment(
                                R.id.fragment_container,
                                fr,
                                fr.fragmentMainTag
                            )
                        }
                    }
                    R.id.navigation_favourites -> {
                        if (fragment !is FavouritesFragment) {

                            val fr = FavouritesFragment.newInstance()

                            supportFragmentManager.clearAndReplaceFragment(
                                R.id.fragment_container,
                                fr,
                                fr.fragmentMainTag
                            )
                        }
                    }
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun fragmentLifeCycleController(tag: String) {
        val bottomNavVisiblityFlag = MainFragmentHelper.isShowBottomNavBar(tag)
        val bottomNavPosFlag = MainFragmentHelper.getBottomNavPos(tag)
        val statusBarFlag = MainFragmentHelper.getStatusBarColorRes(tag)

        bottomNavMain.visible(bottomNavVisiblityFlag)

        if (bottomNavPosFlag != null) {
            bottomNavMain.setChecked(bottomNavPosFlag)
        } else {
            bottomNavMain.uncheckAllItems()
        }

        if (statusBarFlag != null) {
            changeStatusBarColor(statusBarFlag)
        } else {
            changeStatusBarColor(R.color.colorAccent)
        }
    }

}
