package thousand.group.aviatatesttask.views.view_models

import android.content.Context
import android.os.Bundle
import thousand.group.aviatatesttask.global.base.BaseViewModel
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.views.repositories.activity.MainRepository

class MainViewModel(
    override val context: Context,
    val repository: MainRepository,
    override val resourceManager: ResourceManager,
    override val globalReceiver: GlobalReceiver
) : BaseViewModel(
    context,
    resourceManager,
    globalReceiver,
    intentActionsList = mutableListOf()
) {
    override fun internetSuccess() {

    }

    override fun internetError() {
    }

    override fun parseBundle(args: Bundle?) {
    }

    override fun onStart() {
    }

    override fun onFinish() {
    }

    override fun onResumed() {
    }

}