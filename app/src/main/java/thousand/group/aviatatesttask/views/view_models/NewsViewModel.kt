package thousand.group.aviatatesttask.views.view_models

import android.content.Context
import android.os.Bundle
import thousand.group.aviatatesttask.global.base.BaseViewModel
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.global.system.SingleLiveEvent
import thousand.group.aviatatesttask.models.global.NewsModel
import thousand.group.aviatatesttask.views.repositories.news.NewsRepository

class NewsViewModel(
    override val context: Context,
    val repository: NewsRepository,
    override val resourceManager: ResourceManager,
    override val globalReceiver: GlobalReceiver,
    override var args: Bundle?
) : BaseViewModel(
    context,
    resourceManager,
    globalReceiver,
    args,
    intentActionsList = mutableListOf(
        AppConstants.LocalizedActionConstants.OPEN_DETAIL_FRAGMENT
    )
) {

    val openDetailFragmentLV = SingleLiveEvent<NewsModel>()

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

    override fun parseBundle(args: Bundle?) {
    }

    override fun onStart() {
        setGlobalReceiverCallback {
            when (it.action) {
                AppConstants.LocalizedActionConstants.OPEN_DETAIL_FRAGMENT -> {
                    it.getParcelableExtra<NewsModel>(AppConstants.LocalizedActionConstants.OPEN_DETAIL_FRAGMENT)
                        ?.apply {
                            openDetailFragmentLV.postValue(this)
                        }
                }
            }
        }

    }

    override fun onFinish() {
    }

    override fun onResumed() {
    }

}