package thousand.group.aviatatesttask.global.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import thousand.group.aviatatesttask.models.global.SourceModel

class DataConverter {
    @TypeConverter
    fun fromSourceModel(data: SourceModel?): String? {
        if (data == null) return null
        val type = object : TypeToken<SourceModel?>() {}.type
        return Gson().toJson(data, type)
    }

    @TypeConverter
    fun toSourceModel(dataString: String?): SourceModel? {
        if (dataString.isNullOrEmpty()) return null
        val type = object : TypeToken<SourceModel?>() {}.type
        return Gson().fromJson(dataString, type)
    }
}
