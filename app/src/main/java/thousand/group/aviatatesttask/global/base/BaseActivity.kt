package thousand.group.aviatatesttask.global.base

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.extentions.observeLiveData
import thousand.group.aviatatesttask.global.helpers.LocaleHelper
import thousand.group.aviatatesttask.global.helpers.MessageStatusHelper
import thousand.group.aviatatesttask.global.helpers.ProgressBarHelper
import kotlin.reflect.KClass

abstract class BaseActivity<out ViewModelType : BaseViewModel>(clazz: KClass<ViewModelType>) :
    AppCompatActivity() {

    var activityTag: String = this.javaClass.simpleName

    abstract var layoutResId: Int

    protected val viewModel: ViewModelType by viewModel(clazz) {
        parametersOf(this)
    }

    private var startFlag = false

    private val messageHelper: MessageStatusHelper by inject {
        parametersOf(this)
    }
    private val progressHelper: ProgressBarHelper by inject {
        parametersOf(this)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            if (startFlag) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            } else {
                startFlag = true
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)

        lifecycle.addObserver(viewModel)

        setStandartLiveDataCallback()
        registerFragmentLifecycle()

        initIntent(intent)
        initView(savedInstanceState)
        initLiveData()
        initController()

        startFlag = true

    }

    override fun onDestroy() {
        viewModelStore.clear()
        lifecycle.removeObserver(viewModel)
        super.onDestroy()
    }

    override fun attachBaseContext(newBase: Context?) {
        newBase?.apply {
            try {
                super.attachBaseContext(LocaleHelper(this).wrap())
            } catch (e: ClassCastException) {
                e.printStackTrace()
            }
        }
    }
//
//    override fun onNewIntent(intent: Intent?) {
//        super.onNewIntent(intent)
//        presenter.parseIntent(intent)
//        presenter.checkAction()
//    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {

        overrideConfiguration?.let {
            val uiMode = it.uiMode
            it.setTo(baseContext.resources.configuration)
            it.uiMode = uiMode
        }

        super.applyOverrideConfiguration(overrideConfiguration)
    }

    protected fun closeKeyboard() {
        currentFocus?.apply {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(windowToken, 0)
        }
    }

    protected fun openKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun changeStatusBarColor(colorRess: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val color = ContextCompat.getColor(this, colorRess)

            val window = window

            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (colorRess == R.color.colorWhite) {
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                } else {
                    window.decorView.systemUiVisibility = 0
                }
            }
            window.statusBarColor = color
        }
    }

    protected fun setStandartLiveDataCallback() {
        viewModel.apply {

            observeLiveData(successStrLV) {
                messageHelper.showDialogMessageSuccess(it)
            }

            observeLiveData(successIntLV) {
                messageHelper.showDialogMessageSuccess(it)
            }

            observeLiveData(errorStrLV) {
                messageHelper.showDialogMessageError(it)
            }

            observeLiveData(errorIntLV) {
                messageHelper.showDialogMessageError(it)
            }

            observeLiveData(progressLV) {
                if (it) {
                    progressHelper.showProgress()
                } else {
                    progressHelper.hideProgress()
                }
            }

            observeLiveData(toastStrLV) {
                Toast.makeText(this@BaseActivity, it, Toast.LENGTH_SHORT).show()
            }

            observeLiveData(toastIntLV) {
                Toast.makeText(this@BaseActivity, it, Toast.LENGTH_SHORT).show()
            }

            observeLiveData(openKeyboardLV) {
                openKeyboard()
            }

            observeLiveData(closeKeyboardLV) {
                closeKeyboard()
            }
            observeLiveData(openNavBar) {
                openNavBar()
            }
        }
    }

    private fun registerFragmentLifecycle() {
        supportFragmentManager.registerFragmentLifecycleCallbacks(
            object : FragmentManager.FragmentLifecycleCallbacks() {

                override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {
                    super.onFragmentStarted(fm, f)

                    f.tag?.apply {
                        Log.i(activityTag, this)
                        fragmentLifeCycleController(this)
                    }
                }

                override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
                    super.onFragmentPaused(fm, f)

                    if (!fm.fragments.isNullOrEmpty()) {
                        val tag = fm.fragments[fm.fragments.size - 1].tag

                        tag?.apply {
                            Log.i(activityTag, this)
                            fragmentLifeCycleController(this)
                        }
                    }
                }
            },
            true
        )
    }

    fun openNavBar() {
//        drawerLayout?.apply {
//            openDrawer(GravityCompat.START)
//        }
    }

    abstract fun initIntent(intent: Intent?)

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()

    abstract fun fragmentLifeCycleController(tag: String)
}