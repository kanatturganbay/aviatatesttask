package thousand.group.aviatatesttask.global.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import thousand.group.aviatatesttask.models.global.NewsModel

@Dao
interface DataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNewsModel(newsModel: NewsModel): Completable

    @Query("Delete from news_model where publishedAt =:publishedDate")
    fun deleteNewsModel(publishedDate: String): Completable

    @Query("Select count(*) from news_model where  publishedAt =:publishedDate")
    fun getCountOfNewsModel(publishedDate: String): Single<Int>

    @Query("Select * from news_model")
    fun getAllNewsModels(): Single<List<NewsModel>>

}