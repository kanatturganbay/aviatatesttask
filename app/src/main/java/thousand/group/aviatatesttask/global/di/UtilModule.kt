package thousand.group.aviatatesttask.global.di

import android.content.Context
import org.koin.dsl.module
import thousand.group.aviatatesttask.global.helpers.LocaleHelper
import thousand.group.aviatatesttask.global.helpers.MessageStatusHelper
import thousand.group.aviatatesttask.global.helpers.ProgressBarHelper
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.services.storage_services.LocaleStorage
import thousand.group.aviatatesttask.global.system.AppSchedulers
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.global.system.SchedulersProvider

val utilModule = module {
    single<SchedulersProvider> { AppSchedulers() }
    single { LocaleStorage }

    factory { (context: Context) -> ResourceManager(context) }
    factory { (context: Context) -> MessageStatusHelper(context) }
    factory { (context: Context) -> ProgressBarHelper(context) }
    factory { (context: Context) -> LocaleHelper(context) }
    factory { GlobalReceiver() }

}