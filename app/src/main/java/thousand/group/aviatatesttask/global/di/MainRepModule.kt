package thousand.group.aviatatesttask.global.di

import org.koin.dsl.module
import thousand.group.aviatatesttask.views.repositories.activity.MainRepository
import thousand.group.aviatatesttask.views.repositories.activity.MainRepositoryImpl
import thousand.group.aviatatesttask.views.repositories.favourites.FavouritesRepository
import thousand.group.aviatatesttask.views.repositories.favourites.FavouritesRepositoryImpl
import thousand.group.aviatatesttask.views.repositories.news.NewsRepository
import thousand.group.aviatatesttask.views.repositories.news.NewsRepositoryImpl

val mainRepModule = module {
    single<MainRepository> { MainRepositoryImpl(get(), get(), get()) }
    single<NewsRepository> { NewsRepositoryImpl(get(), get(), get(), get()) }
    single<FavouritesRepository> { FavouritesRepositoryImpl(get(), get(), get(), get()) }
//    single<ChangeProfileDataRepository> { ChangeProfileDataRepositoryImpl(get(), get(), get()) }
//    single<ChangePasswordRepository> { ChangePasswordRepositoryImpl(get(), get(), get()) }
//    single<ChangeLangRepository> { ChangeLangRepositoryImpl(get(), get(), get()) }
//    single<MainOffersRepository> { MainOffersRepositoryImpl(get(), get(), get()) }
//    single<CreateNewOfferRepository> { CreateNewOfferRepositoryImpl(get(), get(), get()) }
}