package thousand.group.aviatatesttask.global.extensions


internal fun String.formPhoneNumberFormat(startNumber:String): String {
    var phone = "$startNumber"
    //+7 ([000]) [000]-[00]-[00]

    for (i in 0 until length) {

        when (i) {
            0 -> {
                phone += " (${get(i)}"

            }
            3 -> {
                phone += ") ${get(i)}"
            }
            6, 8 -> {
                phone += "-${get(i)}"
            }
            else -> {
                phone += get(i)
            }
        }
    }
    return phone

}

//internal fun String.formCardNumber(): String {
//    val sb = StringBuilder()
//
//    forEachIndexed { index, c ->
//        if ((index % 4) == 0 && index != 0) {
//            sb.append(" ")
//        }
//
//        sb.append(c)
//    }
//
//    return sb.toString()
//}

//internal fun String.formCurrencyTengeFormat(context: Context): String {
//    var currency = ""
//    val cleanDecString = getDecimals()
//    if (cleanDecString.isNotEmpty()) {
//        val formatter = DecimalFormat(context.getString(R.string.format_currency))
//        currency = formatter.format(cleanDecString.toBigInteger())
//        currency = currency.replace(",", " ")
//        currency = "${currency} ${context.getString(R.string.field_valuta)}"
//    }
//
//    return currency
//}
//
//internal fun String.getDecimals(): String {
//    return this.replace("[^\\d]".toRegex(), "")
//}