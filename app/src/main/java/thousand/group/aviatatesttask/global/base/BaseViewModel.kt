package thousand.group.aviatatesttask.global.base

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import okhttp3.RequestBody
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.constants.simple.AppConstants.LocalizedActionConstants
import thousand.group.aviatatesttask.global.receivers.GlobalReceiver
import thousand.group.aviatatesttask.global.system.ResourceManager
import thousand.group.aviatatesttask.global.system.SingleLiveEvent
import java.io.Serializable
import java.util.*

abstract class BaseViewModel(
    open val context: Context,
    open val resourceManager: ResourceManager,
    open val globalReceiver: GlobalReceiver,
    open var args: Bundle? = null,
    open var intentActionsList: MutableList<String>? = null
) : ViewModel(), LifecycleObserver {

    var vmTag: String = this.javaClass.simpleName

    val successStrLV = SingleLiveEvent<String>()
    val successIntLV = SingleLiveEvent<Int>()
    val errorStrLV = SingleLiveEvent<String>()
    val errorIntLV = SingleLiveEvent<Int>()
    val progressLV = SingleLiveEvent<Boolean>()
    val toastStrLV = SingleLiveEvent<String>()
    val toastIntLV = SingleLiveEvent<Int>()
    val openKeyboardLV = SingleLiveEvent<Boolean>()
    val closeKeyboardLV = SingleLiveEvent<Boolean>()
    val openNavBar = SingleLiveEvent<Boolean>()

    protected val gson = Gson()
    protected val jsonType = object : TypeToken<JsonObject?>() {}.type
    protected val paramsBundle = Bundle()

    protected val localeCode = Locale.getDefault().language

    private var connManager: ConnectivityManager? = null

    private var internetAccess = false
    private var canDoOnResume = false

    private val filter = IntentFilter()

    private val compositeDisposable = CompositeDisposable()

    private val networkRequest = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    private val networkListener = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            internetSuccess()
            internetAccess = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            internetError()
            internetAccess = false
        }

        override fun onUnavailable() {
            super.onUnavailable()
            internetError()
            internetAccess = false
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        args?.apply {
            parseBundle(args)
        }

        onStart()
        addConnectionCallback()
        registerStandartCallbacks()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        onFinish()
        compositeDisposable.clear()
        unRegisterGlobalReceiver()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        if (canDoOnResume) {
            onResumed()
        } else {
            canDoOnResume = true
        }
    }

    fun showMessageSuccess(message: String) = sendMessageReceiver(
        LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_STR,
        LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_STR,
        message
    )

    fun showMessageSuccess(@StringRes message: Int) = sendMessageReceiver(
        LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_INT,
        LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_INT,
        message
    )

    fun showMessageError(message: String) = sendMessageReceiver(
        LocalizedActionConstants.DIALOG_MESSAGE_ERROR_STR,
        LocalizedActionConstants.DIALOG_MESSAGE_ERROR_STR,
        message
    )

    fun showMessageError(@StringRes message: Int) = sendMessageReceiver(
        LocalizedActionConstants.DIALOG_MESSAGE_ERROR_INT,
        LocalizedActionConstants.DIALOG_MESSAGE_ERROR_INT,
        message
    )

    fun showMessageToast(message: String) = sendMessageReceiver(
        LocalizedActionConstants.TOAST_MESSAGE_STR,
        LocalizedActionConstants.TOAST_MESSAGE_STR,
        message
    )

    fun showMessageToast(@StringRes message: Int) = sendMessageReceiver(
        LocalizedActionConstants.TOAST_MESSAGE_INT,
        LocalizedActionConstants.TOAST_MESSAGE_INT,
        message
    )

    fun openNavDrawer() = sendMessageReceiver(
        LocalizedActionConstants.OPEN_NAV_DRAWER,
        null,
        null
    )

    protected fun Disposable.connect() {
        compositeDisposable.add(this)
    }

    protected fun unRegisterGlobalReceiver() {
        context.apply {
            LocalBroadcastManager
                .getInstance(applicationContext)
                .unregisterReceiver(globalReceiver)
        }
    }

    private fun registerGlobalReceiver(vararg actionTextList: String) {

        actionTextList.forEach {
            filter.addAction(it)
        }

        context.apply {
            LocalBroadcastManager.getInstance(applicationContext).registerReceiver(
                globalReceiver,
                filter
            )
        }
    }

    protected fun <T> sendMessageReceiver(action: String, title: String?, message: T?) {

        context.apply {
            val localIntent = Intent(action)

            title?.apply {
                message?.apply {

                    when (this) {
                        is Parcelable -> {
                            localIntent.putExtra(title, message as Parcelable)
                        }
                        is String -> {
                            localIntent.putExtra(title, message as String)
                        }
                        is Boolean -> {
                            localIntent.putExtra(title, message as Boolean)
                        }
                        is Int -> {
                            localIntent.putExtra(title, message as Int)
                        }
                        is ArrayList<*> -> {
                            localIntent.putExtra(title, message as ArrayList<*>)
                        }
                        is Serializable -> {
                            localIntent.putExtra(title, message as Serializable)
                        }
                    }
                }
            }

            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent)
        }
    }

    protected fun setGlobalReceiverCallback(onReceivedDataListener: (intent: Intent) -> Unit) {
        globalReceiver.setCallback(onReceivedDataListener)
    }

    protected fun addConnectionCallback() {
        context.apply {
            connManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            connManager?.apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    registerDefaultNetworkCallback(networkListener)
                } else {
                    registerNetworkCallback(
                        networkRequest,
                        networkListener
                    )
                }
            }
        }
    }

    protected fun showProgressBar(show: Boolean) = sendMessageReceiver(
        LocalizedActionConstants.PROGRESS_BAR_MESSAGE,
        LocalizedActionConstants.PROGRESS_BAR_MESSAGE,
        show
    )

    protected fun parseRequestBodyMap(mapStr: String) = parseParamsMap<String, RequestBody>(mapStr)

    fun openKeyboard() = sendMessageReceiver(
        LocalizedActionConstants.OPEN_KEYBOARD,
        null,
        null
    )

    fun closeKeyboard() = sendMessageReceiver(
        LocalizedActionConstants.CLOSE_KEYBOARD,
        null,
        null
    )

    fun isInternetActive() = internetAccess

    private fun registerStandartCallbacks() {

        intentActionsList?.forEach {
            filter.addAction(it)
        }

        registerGlobalReceiver(
            LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_INT,
            LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_STR,
            LocalizedActionConstants.DIALOG_MESSAGE_ERROR_INT,
            LocalizedActionConstants.DIALOG_MESSAGE_ERROR_STR,
            LocalizedActionConstants.PROGRESS_BAR_MESSAGE,
            LocalizedActionConstants.TOAST_MESSAGE_INT,
            LocalizedActionConstants.TOAST_MESSAGE_STR,
            LocalizedActionConstants.OPEN_KEYBOARD,
            LocalizedActionConstants.CLOSE_KEYBOARD,
            LocalizedActionConstants.OPEN_NAV_DRAWER
        )

        globalReceiver.setStandartCallback {
            Log.i(vmTag, "action: ${it.action}")

            when (it.action) {
                LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_INT -> {
                    parseStringOrIntLocaleMessage(
                        LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_INT,
                        it.getIntExtra(LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_INT, 0),
                        successIntLV
                    )
                }

                LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_STR -> {
                    parseStringOrIntLocaleMessage(
                        LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_STR,
                        it.getStringExtra(LocalizedActionConstants.DIALOG_MESSAGE_SUCCESS_STR),
                        successStrLV
                    )
                }

                LocalizedActionConstants.DIALOG_MESSAGE_ERROR_INT -> {
                    parseStringOrIntLocaleMessage(
                        LocalizedActionConstants.DIALOG_MESSAGE_ERROR_INT,
                        it.getIntExtra(LocalizedActionConstants.DIALOG_MESSAGE_ERROR_INT, 0),
                        errorIntLV
                    )
                }

                LocalizedActionConstants.DIALOG_MESSAGE_ERROR_STR -> {
                    parseStringOrIntLocaleMessage(
                        LocalizedActionConstants.DIALOG_MESSAGE_ERROR_STR,
                        it.getStringExtra(LocalizedActionConstants.DIALOG_MESSAGE_ERROR_STR),
                        errorStrLV
                    )
                }

                LocalizedActionConstants.PROGRESS_BAR_MESSAGE -> {
                    it.getBooleanExtra(LocalizedActionConstants.PROGRESS_BAR_MESSAGE, false).apply {
                        progressLV.postValue(this)
                    }
                }

                LocalizedActionConstants.TOAST_MESSAGE_INT -> {
                    parseStringOrIntLocaleMessage(
                        LocalizedActionConstants.TOAST_MESSAGE_INT,
                        it.getIntExtra(LocalizedActionConstants.TOAST_MESSAGE_INT, 0),
                        toastIntLV
                    )
                }

                LocalizedActionConstants.TOAST_MESSAGE_STR -> {
                    parseStringOrIntLocaleMessage(
                        LocalizedActionConstants.TOAST_MESSAGE_STR,
                        it.getStringExtra(LocalizedActionConstants.TOAST_MESSAGE_STR),
                        toastStrLV
                    )
                }

                LocalizedActionConstants.OPEN_KEYBOARD -> {
                    openKeyboardLV.postValue(true)
                }
                LocalizedActionConstants.CLOSE_KEYBOARD -> {
                    closeKeyboardLV.postValue(true)
                }
                LocalizedActionConstants.OPEN_NAV_DRAWER -> {
                    openNavBar.postValue(true)
                }
            }
        }
    }

    private fun <T> parseStringOrIntLocaleMessage(
        key: String,
        value: T?,
        singleLiveEvent: SingleLiveEvent<T>
    ) {
        value?.apply {
            if (this is Int) {
                if (value != 0) {
                    singleLiveEvent.postValue(value)
                }
            } else if (this is String) {
                singleLiveEvent.postValue(value)
            }
        }
    }

    private fun <K, V> parseParamsMap(mapStr: String): MutableMap<K, V> {
        val mapType = object : TypeToken<MutableMap<K, V>>() {}.type
        val parsedMap = Gson().fromJson<MutableMap<K, V>>(mapStr, jsonType)
        return parsedMap
    }


    abstract fun internetSuccess()

    abstract fun internetError()

    abstract fun parseBundle(args: Bundle?)

    abstract fun onStart()

    abstract fun onFinish()

    abstract fun onResumed()


}

