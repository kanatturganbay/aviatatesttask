package thousand.group.aviatatesttask.global.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.reflect.KClass

abstract class BaseDialogFragment<out ViewModelType : BaseViewModel>(clazz: KClass<ViewModelType>) :
    DialogFragment() {

    var fragmentTag: String = this.javaClass.simpleName

    abstract var layoutResId: Int
    abstract var fragmentMainTag: String

    protected val viewModel: ViewModelType by viewModel(clazz) {
        parametersOf(requireActivity(), arguments)
    }

    override fun onStart() {
        super.onStart()

        dialog?.apply {
            window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(layoutResId, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycle.addObserver(viewModel)

        initView(savedInstanceState)
        initLiveData()
        initController()

    }

    override fun onDestroy() {
        super.onDestroy()

        lifecycle.removeObserver(viewModel)

        viewModelStore.clear()
    }

    fun openNavDrawer() = viewModel.openNavDrawer()

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()

}