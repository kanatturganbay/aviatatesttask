package thousand.group.aviatatesttask.global.extentions

import androidx.core.view.get
import com.google.android.material.navigation.NavigationView

fun NavigationView.setChecked(pos: Int) {
    menu.setGroupCheckable(0, true, false)

    uncheckAllItems()
    menu.get(pos).isChecked = true

    menu.setGroupCheckable(0, true, true)
}

fun NavigationView.uncheckAllItems() {
    menu.setGroupCheckable(0, true, false)

    for (i in 0 until menu.size()) {
        menu.getItem(i).isChecked = false
    }

    menu.setGroupCheckable(0, true, true)
}