package thousand.group.aviatatesttask.global.sealed_classes

sealed class MessageStatuses {
    internal object ON_SUCCESS : MessageStatuses()
    internal object ON_ERROR : MessageStatuses()
}