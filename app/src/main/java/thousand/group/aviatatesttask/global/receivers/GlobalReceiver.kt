package thousand.group.aviatatesttask.global.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class GlobalReceiver : BroadcastReceiver() {

    companion object {
        internal const val LOG_TAG = "GlobalReceiver"
    }

    private lateinit var onReceivedDataListener: (intent: Intent) -> Unit
    private lateinit var onStandartVReceivedMDataListener: (intent: Intent) -> Unit

    override fun onReceive(context: Context?, intent: Intent?) {
        if (::onReceivedDataListener.isInitialized) {
            intent?.apply {
                onReceivedDataListener.invoke(intent)
            }
        }

        if (::onStandartVReceivedMDataListener.isInitialized) {
            intent?.apply {
                onStandartVReceivedMDataListener.invoke(intent)
            }
        }
    }

    fun setCallback(onReceivedDataListener: (intent: Intent) -> Unit) {
        this.onReceivedDataListener = onReceivedDataListener
    }

    fun setStandartCallback(onReceivedDataListener: (intent: Intent) -> Unit) {
        this.onStandartVReceivedMDataListener = onReceivedDataListener
    }
}