package thousand.group.aviatatesttask.global.extentions


fun Double.roundTo(): Double {
    return String.format("%.2f", this).replace(",", ".").toDouble()
}


