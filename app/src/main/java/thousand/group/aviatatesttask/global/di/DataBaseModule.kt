package thousand.group.aviatatesttask.global.di

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import thousand.group.aviatatesttask.global.db.data_base_util.DataBaseUtil

val dataBaseModule = module {
    single { DataBaseUtil.create(androidContext()) }
}