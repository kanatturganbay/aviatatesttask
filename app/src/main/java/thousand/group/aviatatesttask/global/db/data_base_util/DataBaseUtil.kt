package thousand.group.aviatatesttask.global.db.data_base_util

import android.content.Context
import android.database.sqlite.SQLiteException
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import thousand.group.aviatatesttask.global.db.daos.DataDao
import thousand.group.aviatatesttask.global.extentions.ioThread
import thousand.group.aviatatesttask.models.global.NewsModel
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader

@Database(
    entities = [
        NewsModel::class
    ],
    version = 1,
    exportSchema = false
)
abstract class DataBaseUtil: RoomDatabase(){
    abstract fun getDataDao(): DataDao

    companion object{

        private const val TAG = "DataBaseUtil"

        private const val SYSTEM_DEF_VALUE = "system_value"

        fun create(context: Context): DataBaseUtil =
            Room.databaseBuilder(
                context,
                DataBaseUtil::class.java,
                DataBaseProperties.DB_NAME
            )
                .addCallback(object: Callback(){
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
//                        val raw = context.resources
//                            .openRawResource(
//                                context.resources
//                                    .getIdentifier(
//                                        SYSTEM_DEF_VALUE,
//                                        "raw",
//                                        context.packageName)
//                            )
//
//                        ioThread{
//                            val reader = BufferedReader(InputStreamReader(raw, "UTF8") as Reader?)
//                            try {
//                                reader.readText().split(";").forEach { db.execSQL(it) }
//                            }catch (e: SQLiteException){
//                                e.printStackTrace()
//                            }
//
//                        }
                    }
                })
                .build()
    }

    object DataBaseProperties{
        val DB_NAME = "news.db"
    }
}