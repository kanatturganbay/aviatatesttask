package thousand.group.aviatatesttask.global.constants.simple

object Endpoints {
    const val GET_TOP_NEWS = "v2/top-headlines"
    const val GET_ALL_NEWS = "v2/everything"
}