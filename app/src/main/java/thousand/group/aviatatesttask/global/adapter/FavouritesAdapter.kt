package thousand.group.aviatatesttask.global.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_favourite.view.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.models.global.NewsModel

class FavouritesAdapter(
    val context: Context,
    val itemClickListener: (model: NewsModel) -> Unit,
    val itemDeleteClicked: (model: NewsModel, position: Int) -> Unit,
    val itemEmptyCallback: (isEmpty: Boolean) -> Unit
) : RecyclerSwipeAdapter<FavouritesAdapter.ViewHolder>() {

    private var dataList = mutableListOf<NewsModel>()
    private var vmTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_favourite, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mItemManger.bindView(holder.itemView, position)
        holder.bind(getItem(position), position)
    }

    override fun getSwipeLayoutResourceId(position: Int): Int = R.id.swipe_main_layout

    override fun getItemCount(): Int = dataList.size

    fun setDataList(list: MutableList<NewsModel>) {
        this.dataList.clear()
        this.dataList.addAll(list)

        notifyDataSetChanged()

        itemEmptyCallback.invoke(this.dataList.isEmpty())
    }

    fun getItem(position: Int) = dataList[position]

    fun getList() = dataList

    fun deleteItem(position: Int) {
        dataList.removeAt(position)

        notifyItemRemoved(position)
        notifyItemRangeChanged(position, dataList.size)

        itemEmptyCallback.invoke(this.dataList.isEmpty())

        mItemManger.closeAllItems()
    }

    fun closeBasketItem(position: Int) = mItemManger.closeItem(position)

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(news: NewsModel, position: Int) {
            itemView.apply {
                Picasso
                    .get()
                    .load(news.urlToImage)
                    .error(R.drawable.ic_error)
                    .resizeDimen(R.dimen._50sdp, R.dimen._50sdp)
                    .into(news_image)


                news_name.setText(news.title)

                swipe_main_layout.addDrag(SwipeLayout.DragEdge.Right, bottom_wrapper)

                swipe_main_layout.onSwipeStartOpen {
                    news.isOpened = true
                }

                main_layout.setOnClickListener {
                    itemClickListener.invoke(news)
                }

                swipe_main_layout.onSwipeHandRelease { layout, xvel, yvel ->
                    if (news.isOpened) {
                        itemDeleteClicked.invoke(news, position)
                        news.isOpened = false
                    }
                }
            }
        }
    }

}