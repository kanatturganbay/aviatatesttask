package thousand.group.ayanproject.global.services.api_services

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.global.constants.simple.Endpoints
import thousand.group.aviatatesttask.models.global.NewsTotalResult
import thousand.group.aviatatesttask.models.global.Pagination

interface ServerService {
    @GET(Endpoints.GET_TOP_NEWS)
    fun getTopNews(
        @Query(AppConstants.ApiFieldConstants.page) page: String,
        @Query(AppConstants.ApiFieldConstants.apiKey) apiKey: String,
        @Query(AppConstants.ApiFieldConstants.country) country: String
    ): Single<NewsTotalResult>

    @GET(Endpoints.GET_ALL_NEWS)
    fun getAllNews(
        @Query(AppConstants.ApiFieldConstants.page) page: String,
        @Query(AppConstants.ApiFieldConstants.apiKey) apiKey: String,
        @Query(AppConstants.ApiFieldConstants.q) q: String
    ): Single<NewsTotalResult>

//
//    @Multipart
//    @POST(Endpoints.SIGN_IN)
//    fun login(
//        @PartMap params: MutableMap<String, RequestBody>
//    ): Single<Response<LoginObject>>
//
//    @Multipart
//    @POST(Endpoints.SIGN_UP)
//    fun signUp(
//        @PartMap params: MutableMap<String, RequestBody>
//    ): Single<Response<LoginObject>>
//
//    @GET(Endpoints.GET_CITIES)
//    fun getCities(): Single<Response<MutableList<City>>>
//
//    @Multipart
//    @POST(Endpoints.GET_USER_BY_TOKEN)
//    fun getUserByToken(
//        @PartMap params: MutableMap<String, RequestBody>
//    ): Single<Response<User>>
//
//    @Multipart
//    @POST(Endpoints.EDIT_PROFILE)
//    fun editProfile(
//        @PartMap params: MutableMap<String, RequestBody>,
//        @Part image: MultipartBody.Part?
//    ): Single<Response<LoginObject>>


//    @Multipart
//    @POST(Endpoints.SIGN_IN)
//    fun signIn(@PartMap params: MutableMap<String, RequestBody>): Single<Response<User>>
//
//    @GET(Endpoints.SIGN_OUT)
//    fun signOut(): Completable
//
//    @GET(Endpoints.AUTH)
//    fun auth(
//        @Query(UserRequest.device_token) device_token: String,
//        @Query(UserRequest.device_type) device_type: String
//    ): Single<Response<User>>
//
//    @GET(Endpoints.GET_ORDERS)
//    fun getOrders(@Query(GoodRequest.page) page: String): Single<Pagination>
//
//    @GET(Endpoints.GET_ORDER_DETAIL)
//    fun getOrderDetail(@Path(GoodRequest.id) id: String): Single<Response<OrderDetail>>
//
//    @FormUrlEncoded
//    @POST(Endpoints.SET_ORDER_STATUS)
//    fun setOrderStatus(
//        @Path(GoodRequest.id) id: String,
//        @Field(GoodRequest.delivered) delivered: String
//    ): Single<Response<OrderDetail>>
//
//    @Multipart
//    @POST(Endpoints.PROFILE_EDIT)
//    fun profileEdit(
//        @PartMap params: MutableMap<String, RequestBody>,
//        @Part image: MultipartBody.Part?
//    ): Single<Response<User>>
//
//    @GET(Endpoints.GET_COMMENTS)
//    fun getComments(
//        @Query(GoodRequest.page) page: String
//    ): Single<Pagination>
//
//    @GET(Endpoints.GET_CITIES)
//    fun getCities(): Single<Response<MutableList<City>>>
//
//    @GET(Endpoints.ACCEPT_ORDER)
//    fun acceptOrder(@Path(GoodRequest.id) id: String): Single<Response<OrderDetail>>
//
//    @GET("pickupAddress")
//    fun getAddress():Single<Response<JsonObject>>


}