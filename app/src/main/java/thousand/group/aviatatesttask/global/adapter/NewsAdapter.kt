package thousand.group.aviatatesttask.global.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_news.view.*
import thousand.group.aviatatesttask.R
import thousand.group.aviatatesttask.global.constants.simple.AppConstants
import thousand.group.aviatatesttask.models.global.NewsModel

class NewsAdapter(
    val context: Context,
    val OnItemClickListener: (NewsModel, Int) -> Unit,
    val onItemHeartClickListener: (NewsModel, Int) -> Unit,
    val OnBottomReachedListener: () -> Unit,
    val onEmptyListener: (show: Boolean) -> Unit
) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    private var dataList = mutableListOf<NewsModel>()
    private var vmTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == dataList.size - AppConstants.PAGE_LIMIT) OnBottomReachedListener.invoke()
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<NewsModel>) {
        this.dataList.clear()
        this.dataList.addAll(dataList)
        notifyDataSetChanged()

        onEmptyListener.invoke(this.dataList.isEmpty())
    }

    fun getItem(position: Int): NewsModel = dataList[position]

    fun checkMarked(position: Int, model: NewsModel) {
        dataList.set(position, model)
        notifyItemChanged(position)
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(news: NewsModel, position: Int) {
            itemView.apply {
                Picasso
                    .get()
                    .load(news.urlToImage)
                    .error(R.drawable.ic_error)
                    .resizeDimen(R.dimen._150sdp, R.dimen._120sdp)
                    .into(banner_ic)

                val checkedIconRes = if (news.isChecked) {
                    R.drawable.checked_heart_drawable
                } else {
                    R.drawable.checked_non_heart_drawable
                }

                checkable_icon.setImageResource(checkedIconRes)

                text_about.setText(news.description)

                title.setText(news.title)

                if (!news.publishedAt.isNullOrEmpty()) {

                    creation_date_value.setText(
                        news.publishedAt
                    )
                } else {
                    creation_date_value.setText(R.string.label_no_date)
                }

                setOnClickListener {
                    OnItemClickListener.invoke(news, position)
                }

                findViewById<ImageView>(R.id.checkable_icon).setOnClickListener {
                    onItemHeartClickListener.invoke(news, position)
                }

            }
        }
    }

}