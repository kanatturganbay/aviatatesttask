package thousand.group.aviatatesttask.global.constants.simple

object AppConstants {

    object ApiValues {
        //        const val api_key = "e65ee0938a2a43ebb15923b48faed18d"
        const val api_key = "e65ee0938a2a43ebb15923b48faed18d"
        const val country = "us"
        const val q = "bitcoin"
    }

    const val PAGE_LIMIT = 5
    const val MESSAGE = "message"

    object CameraActionConstants {
        const val GALLERY_PHOTO_REQUEST_CODE = 10
        const val CAMERA_PHOTO_REQUEST_CODE = 11
        const val DELETE_PHOTO_REQUEST_CODE = 12

        const val ON_LOAD_SUCCESS = "ON_LOAD_SUCCESS"
        const val ON_LOAD_ERROR = "ON_LOAD_ERROR"
        const val ON_DELETE_CLICKED = "ON_DELETE_CLICKED"
    }

    object StatusCodeConstants {
        const val STATUS_CODE_200 = 200
        const val STATUS_CODE_400 = 400
        const val STATUS_CODE_401 = 401
        const val STATUS_CODE_402 = 402
        const val STATUS_CODE_404 = 404
    }

    object LocalizedActionConstants {
        const val DIALOG_MESSAGE_SUCCESS_INT = "DIALOG_MESSAGE_SUCCESS_INT"
        const val DIALOG_MESSAGE_SUCCESS_STR = "DIALOG_MESSAGE_SUCCESS_STR"
        const val DIALOG_MESSAGE_ERROR_INT = "DIALOG_MESSAGE_ERROR_INT"
        const val DIALOG_MESSAGE_ERROR_STR = "DIALOG_MESSAGE_ERROR_STR"
        const val TOAST_MESSAGE_INT = "TOAST_MESSAGE_INT"
        const val TOAST_MESSAGE_STR = "TOAST_MESSAGE_STR"
        const val PROGRESS_BAR_MESSAGE = "SHOW_PROGRESS_BAR"
        const val OPEN_KEYBOARD = "OPEN_KEYBOARD"
        const val CLOSE_KEYBOARD = "CLOSE_KEYBOARD"
        const val OPEN_NAV_DRAWER = "OPEN_NAV_DRAWER"

        const val OPEN_DETAIL_FRAGMENT = "OPEN_DETAIL_FRAGMENT"
    }

    object BundleConstants {
        const val BUNDLE_MODEL = "BUNDLE_MODEL"

        const val AVATAR = "AVATAR"
        const val TITLE = "TITLE"
        const val DESC = "DESC"
        const val AUTHOR = "AUTHOR"
        const val CONTENT = "CONTENT"
        const val POSITION = "POSITION"

    }

    object ApiFieldConstants {
        const val country = "country"
        const val apiKey = "apiKey"
        const val page = "page"
        const val q = "q"
    }
}