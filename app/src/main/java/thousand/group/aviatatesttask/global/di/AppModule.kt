package thousand.group.aviatatesttask.global.di

val appModule = listOf(
    networkModule,
    dataBaseModule,
    utilModule,
    mainRepModule,
    mainVmModule
)