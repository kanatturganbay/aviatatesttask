package thousand.group.aviatatesttask.global.di

import android.content.Context
import android.os.Bundle
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import thousand.group.aviatatesttask.views.view_models.*

val mainVmModule = module {
    viewModel { (context: Context) ->
        MainViewModel(context, get(), get { parametersOf(context) }, get())
    }

    viewModel { (context: Context, args: Bundle?) ->
        NewsViewModel(context, get(), get { parametersOf(context) }, get(), args)
    }

    viewModel { (context: Context, args: Bundle?) ->
        TopNewsViewModel(context, get(), get { parametersOf(context) }, get(), args)
    }

    viewModel { (context: Context, args: Bundle?) ->
        AllNewsViewModel(context, get(), get { parametersOf(context) }, get(), args)
    }

    viewModel { (context: Context, args: Bundle?) ->
        NewsDetailViewModel(context, get { parametersOf(context) }, get(), args)
    }

    viewModel { (context: Context, args: Bundle?) ->
        FavouritesViewModel(context, get(), get { parametersOf(context) }, get(), args)
    }

//
//    viewModel { (context: Context, args: Bu`ndle?) ->
//        ProfileViewModel(context, get(), get { parametersOf(context) }, get(), args)
//    }
//
//    viewModel { (context: Context, args: Bundle?) ->
//        ChangeProfileDataViewModel(context, get(), get { parametersOf(context) }, get(), args)
//    }
//
//    viewModel { (context: Context, args: Bundle?) ->
//        PhotoSelectionViewModel(context, get { parametersOf(context) }, get(), args)
//    }
//
//    viewModel { (context: Context, args: Bundle?) ->
//        ChangePasswordViewModel(context, get(), get { parametersOf(context) }, get(), args)
//    }
//
//    viewModel { (context: Context, args: Bundle?) ->
//        ChangeLangViewModel(context, get(), get { parametersOf(context) }, get(), args)
//    }
//
//    viewModel { (context: Context, args: Bundle?) ->
//        MainOffersViewModel(context, get(), get { parametersOf(context) }, get(), args)
//    }
//
//    viewModel { (context: Context, args: Bundle?) ->
//        CreateNewOfferViewModel(context, get(), get { parametersOf(context) }, get(), args)
//    }
}